package br.com.senac.model;

public class Usuario {

    private String user;
    private String senha;

    public Usuario() {
    }

    public Usuario(String user, String senha) {
        this.user = user;
        this.senha = senha;
    }

    public String getNome() {
        return user;
    }

    public void setNome(String user) {
        this.user = user;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

}
